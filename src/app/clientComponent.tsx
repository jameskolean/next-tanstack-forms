/// <reference types="../../node_modules/@types/react-dom/experimental" />
"use client"

import { useFormState } from "react-dom"
import { mergeForm, useTransform } from "@tanstack/react-form"
import { exampleAction } from "./actions"
import { exampleFormFactory, exampleValidationSchema } from "./forms"
import type { FormApi } from "@tanstack/react-form"
import { zodValidator } from "@tanstack/zod-form-adapter"
export const ClientComp = () => {
  const [state, action] = useFormState(exampleAction, exampleFormFactory.initialFormState)

  const form = exampleFormFactory.useForm({
    transform: useTransform((baseForm: FormApi<any, any>) => mergeForm(baseForm, state), [state]),
  })

  const formErrors = form.useStore((formState) => formState.errors)

  return (
    <form action={action as never} onSubmit={() => form.handleSubmit()}>
      {formErrors.map((error) => (
        <p key={error as string}>{error}</p>
      ))}
      <p>{JSON.stringify(state)}</p>
      <form.Field
        name="firstName"
        validatorAdapter={zodValidator}
        validators={{
          onChange: exampleValidationSchema?.shape.firstName,
        }}
      >
        {(field) => {
          return (
            <div>
              <input name="firstName" value={field.state.value} onChange={(e) => field.handleChange(e.target.value)} />
              {field.state.meta.errors.map((error) => (
                <p key={error as string}>{error}</p>
              ))}
            </div>
          )
        }}
      </form.Field>
      <form.Field name="age">
        {(field) => {
          return (
            <div>
              <input
                name="age"
                type="number"
                value={field.state.value}
                onChange={(e) => field.handleChange(e.target.valueAsNumber)}
              />
              {field.state.meta.errors.map((error) => (
                <p key={error as string}>{error}</p>
              ))}
            </div>
          )
        }}
      </form.Field>
      <form.Subscribe selector={(formState) => [formState.canSubmit, formState.isSubmitting]}>
        {([canSubmit, isSubmitting]) => (
          <button type="submit" disabled={!canSubmit}>
            {isSubmitting ? "..." : "Submit"}
          </button>
        )}
      </form.Subscribe>
    </form>
  )
}
