import { createFormFactory } from "@tanstack/react-form";
import { zodValidator } from "@tanstack/zod-form-adapter";
import { z } from "zod";

export const exampleValidationSchema = z.object({
  age: z.number().min(9),
  firstName: z.string().min(3),
});
export const exampleFormFactory = createFormFactory({
  defaultValues: {
    firstName: "",
    age: 0,
  },
  validatorAdapter: zodValidator,
  validators: { onChange: exampleValidationSchema },
  onServerValidate: exampleValidationSchema,
});
