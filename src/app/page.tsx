import styles from "./page.module.css";
import { ClientComp } from "./clientComponent";
export default function Home() {
  return (
    <main className={styles.main}>
      <ClientComp />
    </main>
  );
}
