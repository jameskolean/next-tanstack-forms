"use server";
import { exampleFormFactory } from "./forms";

export async function exampleAction(prev: unknown, formData: FormData) {
  const validation = await exampleFormFactory.validateFormData(formData, {
    numbers: ["age"],
  });
  if (validation.errors && validation.errors.length === 0) {
    console.log("do stuff");
    return { ...validation, success: true };
  }
  return { ...validation, success: false };
}
